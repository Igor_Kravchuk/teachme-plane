import UIKit

//MARK: - ENUMS

class ViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var nameVCLabel: UILabel?
    @IBOutlet weak var playButton: UIButton?
    @IBOutlet weak var recordsButton: UIButton?
    @IBOutlet weak var settingsButton: UIButton?
    
    //MARK: - VARS
    
    //MARK: - LETS
    
    //MARK: - Life functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createAttributedGameName()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.addCornerRadiusToButtons()
        self.addShadowsToButtons()
    }
    
    //MARK: - IBActions
    @IBAction func playGameButtonPressed(_ sender: UIButton) {
        self.playGame()
    }
    
    @IBAction func goToRecords(_ sender: UIButton) {
        let nextController = self.storyboard?.instantiateViewController(withIdentifier: "RecordsViewController") as! RecordsViewController
        nextController.delegate = self
        self.navigationController?.pushViewController(nextController, animated: true)
    }
    
    @IBAction func goToSettings(_ sender: UIButton) {
        let nextController = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        self.navigationController?.pushViewController(nextController, animated: true)
    }
    
    //MARK: - Flow functions
    func playGame() {
        let nextController = self.storyboard?.instantiateViewController(withIdentifier: "GameViewController") as! GameViewController
        self.navigationController?.pushViewController(nextController, animated: true)
    }
    
    func addCornerRadiusToButtons() {
        if let playButton = self.playButton,
           let recordsButton = self.recordsButton,
           let settingsButton = self.settingsButton {
            playButton.changeCornerRadius()
            recordsButton.changeCornerRadius()
            settingsButton.changeCornerRadius()
        }
    }
    
    func addShadowsToButtons() {
        if let playButton = self.playButton,
           let recordsButton = self.recordsButton,
           let settingsButton = self.settingsButton {
            settingsButton.dropShadow()
            recordsButton.dropShadow()
            playButton.dropShadow()
        }
    }
    
    func createAttributedGameName() {
        guard let gameNameLabel = self.nameVCLabel else {
            print("Name View Controller Label is absent")
            return
        }
        let localizedStringFirstPart = "Teach".localized
        let localizedStringSecondPart = "Me".localized
        let localizedStringThirdPart = " - ".localized
        let localizedStringFourthPart = "PLANE".localized
        
        let redAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.foregroundColor: UIColor.red]
        let yellowAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.foregroundColor: UIColor.yellow]
        let whiteAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.foregroundColor: UIColor.white]
        let blackAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.foregroundColor: UIColor.black]
        
        let firstAttributedWord = NSAttributedString(string: localizedStringFirstPart, attributes: blackAttribute)
        let secondAttributedWord = NSAttributedString(string: localizedStringSecondPart, attributes: yellowAttribute)
        let thirdAttributedWord = NSAttributedString(string: localizedStringThirdPart, attributes: whiteAttribute)
        let fourthAttributedWord = NSAttributedString(string: localizedStringFourthPart, attributes: redAttribute)
        let gameName = NSMutableAttributedString(string: "")
        gameName.append(firstAttributedWord)
        gameName.append(secondAttributedWord)
        gameName.append(thirdAttributedWord)
        gameName.append(fourthAttributedWord)
        gameNameLabel.attributedText = gameName
    }
}

extension ViewController: RecordsViewControllerDelegate {
    func goToGameViewController() {
        self.playGame()
    }
}

