import Foundation

class SettingsManager {
    static let shared = SettingsManager()
    
    let easyDifficulty = Difficulty(gameSpeedCoefficient: 1, scoresTimeInterval: 1.5, dangerousPlaneNumber: 1)
    let mediumDifficulty = Difficulty(gameSpeedCoefficient: 1.2, scoresTimeInterval: 1, dangerousPlaneNumber: 1.75)
    let hardDifficulty = Difficulty(gameSpeedCoefficient: 1.5, scoresTimeInterval: 0.75, dangerousPlaneNumber: 2.5)
    
    let localizedDefaultPlayerName = "Player".localized
    let defaultMainPlane = "MainPlane1"
    let defaultControlType = ControlTypes.accelerometer.rawValue
    
    func loadDifficulty() -> Difficulty {
        guard let difficulty = UserDefaults.standard.value(Settings.self, forKey: Keys.settings.rawValue)?.difficulty else {
            return self.easyDifficulty
        }
        return difficulty
    }
    
    func loadSettings() -> Settings {
        let defaultSettings = Settings(playerName: self.localizedDefaultPlayerName, mainPlane: self.defaultMainPlane, difficulty: self.easyDifficulty, controlType: self.defaultControlType)
        guard let settings = UserDefaults.standard.value(Settings.self, forKey: Keys.settings.rawValue) else {
            return defaultSettings
        }
        return settings
    }
}
