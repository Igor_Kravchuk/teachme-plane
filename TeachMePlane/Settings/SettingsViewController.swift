import UIKit

//MARK: - ENUMS
enum Keys: String {
    case settings = "Settings"
    case records = "Records"
}

enum ControlTypes: String {
    case handle = "Handle"
    case accelerometer = "Accelerometer"
}

class SettingsViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var nameViewControllerLabel: UILabel?
    @IBOutlet weak var playerNameTextField: UITextField?
    @IBOutlet weak var firstMainPlaneArea: UIView?
    @IBOutlet weak var secondMainPlaneArea: UIView?
    @IBOutlet weak var thirdMainPlaneArea: UIView?
    @IBOutlet var choosePlaneButtons: [UIButton]?
    @IBOutlet var chooseDifficultyButtons: [UIButton]?
    @IBOutlet var chooseControlTypeButtons: [UIButton]?
    
    //MARK: - VARS
    private var settings: Settings?
    private var playerName: String?
    private var mainPlane: String?
    private var difficulty: Difficulty?
    private var controlType: String?
    
    //MARK: - LETS
    
    //MARK: - Life functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.settings = SettingsManager.shared.loadSettings()
        self.loadSettings()
    }
    
    //MARK: - IBActions
    @IBAction func goToMainMenu(_ sender: UIButton) {
        self.safeSettings()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func chooseMainPlaneButtonPressed(_ sender: UIButton) {
        guard let choosePlaneButtons = self.choosePlaneButtons else {return}
        for button in choosePlaneButtons {
            button.isSelected = false
        }
        sender.isSelected = !sender.isSelected
        self.chooseMainPlane(mainPlaneNumber: sender.tag)
    }
    
    @IBAction func chooseDifficultyButtonPressed(_ sender: UIButton) {
        guard let chooseDifficultyButtons = self.chooseDifficultyButtons else {return}
        for button in chooseDifficultyButtons {
            button.isSelected = false
        }
        sender.isSelected = !sender.isSelected
        self.chooseDifficulty(difficulty: sender.tag)
    }
    
    @IBAction func chooseControlTypeButtonPressed(_ sender: UIButton) {
        guard let chooseControlTypeButtons = self.chooseControlTypeButtons else {return}
        for button in chooseControlTypeButtons {
            button.isSelected = false
        }
        sender.isSelected = !sender.isSelected
        self.chooseControlType(tag: sender.tag)
    }
    
    //MARK: - Flow functions
    private func writePlayerName() {
        let localizedString = "Player".localized
        self.playerName = self.playerNameTextField?.text ?? localizedString
    }
    
    private func safeSettings() {
        self.writePlayerName()
        let settings = Settings(playerName: self.playerName, mainPlane: self.mainPlane, difficulty: self.difficulty, controlType: self.controlType)
        UserDefaults.standard.set(encodable: settings, forKey: Keys.settings.rawValue)
    }
    
    private func loadSettings() {
        guard let settings = self.settings else { return }
        self.playerNameTextField?.text = settings.playerName
        self.mainPlane = settings.mainPlane
        self.difficulty = settings.difficulty
        self.controlType = settings.controlType
        self.changeStartDifficultyBackground()
        self.changeStartMainPlaneBackground()
        self.changeStartControlType()
    }
    
    private func changeStartMainPlaneBackground() {
        guard let choosePlaneButtons = self.choosePlaneButtons else {return}
        switch self.mainPlane {
        case "MainPlane1":
            choosePlaneButtons[0].isSelected = true
        case "MainPlane2":
            choosePlaneButtons[1].isSelected = true
        case "MainPlane3":
            choosePlaneButtons[2].isSelected = true
        default:
            print("ERROR: There is no MainPlane with this number")
        }
    }
    
    private func chooseMainPlane(mainPlaneNumber: Int) {
        switch mainPlaneNumber {
        case 1:
            self.mainPlane = "MainPlane\(mainPlaneNumber)"
        case 2:
            self.mainPlane = "MainPlane\(mainPlaneNumber)"
        case 3:
            self.mainPlane = "MainPlane\(mainPlaneNumber)"
        default:
            print("ERROR: There is no MainPlane with this number")
        }
    }
    
    private func changeStartDifficultyBackground() {
        guard let chooseDifficultyButtons = self.chooseDifficultyButtons else {return}
        if let difficulty = self.difficulty {
            if difficulty.gameSpeedCoefficient == SettingsManager.shared.easyDifficulty.gameSpeedCoefficient,
               difficulty.dangerousPlaneNumber == SettingsManager.shared.easyDifficulty.dangerousPlaneNumber,
               difficulty.scoresTimeInterval == SettingsManager.shared.easyDifficulty.scoresTimeInterval {
                chooseDifficultyButtons[0].isSelected = true
            } else if difficulty.gameSpeedCoefficient == SettingsManager.shared.mediumDifficulty.gameSpeedCoefficient,
                      difficulty.dangerousPlaneNumber == SettingsManager.shared.mediumDifficulty.dangerousPlaneNumber,
                      difficulty.scoresTimeInterval == SettingsManager.shared.mediumDifficulty.scoresTimeInterval {
                chooseDifficultyButtons[1].isSelected = true
            } else if difficulty.gameSpeedCoefficient == SettingsManager.shared.hardDifficulty.gameSpeedCoefficient,
                      difficulty.dangerousPlaneNumber == SettingsManager.shared.hardDifficulty.dangerousPlaneNumber,
                      difficulty.scoresTimeInterval == SettingsManager.shared.hardDifficulty.scoresTimeInterval  {
                chooseDifficultyButtons[2].isSelected = true
            } else {
                chooseDifficultyButtons[0].isSelected = true
                self.difficulty = SettingsManager.shared.easyDifficulty
            }
        }
    }
    
    private func chooseDifficulty(difficulty: Int) {
        switch difficulty {
        case 1:
            self.difficulty = SettingsManager.shared.easyDifficulty
        case 2:
            self.difficulty = SettingsManager.shared.mediumDifficulty
        case 3:
            self.difficulty = SettingsManager.shared.hardDifficulty
        default:
            print("ERROR: There is no difficulty")
        }
    }
    
    private func chooseControlType(tag: Int) {
        switch tag {
        case 1:
            self.controlType = ControlTypes.accelerometer.rawValue
        case 2:
            self.controlType = ControlTypes.handle.rawValue
        default:
            self.controlType = ControlTypes.accelerometer.rawValue
        }
    }
    
    private func changeStartControlType() {
        guard let controlType = self.controlType else {return}
        switch controlType {
        case ControlTypes.accelerometer.rawValue:
            chooseControlTypeButtons?[0].isSelected = true
        case ControlTypes.handle.rawValue:
            chooseControlTypeButtons?[1].isSelected = true
        default:
            print("ERROR: There is no control type with this number")
        }
    }
}

extension SettingsViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
