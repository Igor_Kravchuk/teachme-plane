import UIKit

protocol RecordsAbsentAlertViewDelegate: AnyObject {
    func playButtonPressed()
    func goToMainMenuButtonPressed()
}

    //MARK: - ENUMS
//Test work with Sourcetree (branches)

class RecordsAbsentAlertView: UIView {
    //MARK: - IBOutlets
    @IBOutlet var yesNoButtons: [UIButton]?
    @IBOutlet weak var recordsAbsentAletViewArea: UIView?
    
    //MARK: - VARS
    weak var delegate: RecordsAbsentAlertViewDelegate?
    
    //MARK: - LETS
    
    //MARK: - Life functions
    class func instanceFromNib() -> RecordsAbsentAlertView {
        guard let recordsAbsentAlertView = UINib(nibName: "RecordsAbsentAlertView", bundle: nil).instantiate(withOwner: nil, options: nil).first as? RecordsAbsentAlertView else {
            return RecordsAbsentAlertView()
        }
        return recordsAbsentAlertView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.yesNoButtons?.forEach { (button) in
            button.layer.cornerRadius = 15
        }
        self.recordsAbsentAletViewArea?.layer.cornerRadius = 15
    }
    
    //MARK: - IBActions
    @IBAction func playButtonPressed(_ sender: UIButton) {
        self.delegate?.playButtonPressed()
    }
    @IBAction func goToMainMenuButtonPressed(_ sender: UIButton) {
        self.delegate?.goToMainMenuButtonPressed()
    }
    
    //MARK: - Flow functions
    
}
