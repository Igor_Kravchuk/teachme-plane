import Foundation

class Records: Codable {
    
    var playerName: String?
    var score: Int?
    var date: String?
    
    init(playerName: String?, score: Int?, date: String?) {
        self.playerName = playerName
        self.score = score
        self.date = date
    }
    
    private enum CodingKeys: String, CodingKey {
        case playerName
        case score
        case date
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        playerName = try container.decodeIfPresent(String.self, forKey: .playerName)
        score = try container.decodeIfPresent(Int.self, forKey: .score)
        date = try container.decodeIfPresent(String.self, forKey: .date)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.playerName, forKey: .playerName)
        try container.encode(self.score, forKey: .score)
        try container.encode(self.date, forKey: .date)
    }
    
}
