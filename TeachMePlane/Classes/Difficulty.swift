import Foundation

class Difficulty: Codable {
    
    var gameSpeedCoefficient: Double?
    var scoresTimeInterval: TimeInterval?
    var dangerousPlaneNumber: Double?
    
    init(gameSpeedCoefficient: Double?, scoresTimeInterval: TimeInterval?, dangerousPlaneNumber: Double?) {
        self.gameSpeedCoefficient = gameSpeedCoefficient
        self.scoresTimeInterval = scoresTimeInterval
        self.dangerousPlaneNumber = dangerousPlaneNumber
    }
    
    private enum CodingKeys: String, CodingKey {
        case gameSpeedCoefficient
        case scoresTimeInterval
        case dangerousPlaneNumber
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        gameSpeedCoefficient = try container.decodeIfPresent(Double.self, forKey: .gameSpeedCoefficient)
        scoresTimeInterval = try container.decodeIfPresent(TimeInterval.self, forKey: .scoresTimeInterval)
        dangerousPlaneNumber = try container.decodeIfPresent(Double.self, forKey: .dangerousPlaneNumber)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.gameSpeedCoefficient, forKey: .gameSpeedCoefficient)
        try container.encode(self.scoresTimeInterval, forKey: .scoresTimeInterval)
        try container.encode(self.dangerousPlaneNumber, forKey: .dangerousPlaneNumber)
    }
}
