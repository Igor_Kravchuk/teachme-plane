import Foundation

class Settings: Codable {
    
    var playerName: String?
    var mainPlane: String? = "MainPlane1"
    var difficulty: Difficulty?
    var controlType: String?
    
    init(playerName: String?, mainPlane: String?, difficulty: Difficulty?, controlType: String?) {
        self.playerName = playerName
        self.mainPlane = mainPlane
        self.difficulty = difficulty
        self.controlType = controlType
    }
    
    private enum CodingKeys: String, CodingKey {
        case playerName
        case mainPlane
        case difficulty
        case controlType
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        playerName = try container.decodeIfPresent(String.self, forKey: .playerName)
        mainPlane = try container.decodeIfPresent(String.self, forKey: .mainPlane)
        difficulty = try container.decodeIfPresent(Difficulty.self, forKey: .difficulty)
        controlType = try container.decodeIfPresent(String.self, forKey: .controlType)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(self.playerName, forKey: .playerName)
        try container.encode(self.mainPlane, forKey: .mainPlane)
        try container.encode(self.difficulty, forKey: .difficulty)
        try container.encode(self.controlType, forKey: .controlType)
    }
    
}

