import UIKit

class RecordsTableViewCell: UITableViewCell {
    
//MARK: - IBOutlets
    @IBOutlet weak var numberLabel: UILabel?
    @IBOutlet weak var playerNameLabel: UILabel?
    @IBOutlet weak var dateLabel: UILabel?
    @IBOutlet weak var scoreLabel: UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func configure(obgect: Records) {
        self.playerNameLabel?.text = obgect.playerName
        self.dateLabel?.text = obgect.date
        if let score = obgect.score {
            self.scoreLabel?.text = String(score)
        } else {
            self.scoreLabel?.text = "-"
        }
    }
}
