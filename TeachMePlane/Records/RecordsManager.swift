import Foundation

class RecordsManager {
    static let shared = RecordsManager()
    
    func saveRecords(_ record: Records) {
        var previousRecords = self.loadRecords()
        previousRecords.append(record)
        UserDefaults.standard.set(encodable: previousRecords, forKey: Keys.records.rawValue)
    }
    
    func loadRecords() -> [Records] {
        guard let previousRecords = UserDefaults.standard.value([Records].self, forKey: Keys.records.rawValue) else {
            return []
        }
        return previousRecords
    }
    
    func clearRecords() {
        let clearRecords: [Records] = []
        UserDefaults.standard.set(encodable: clearRecords, forKey: Keys.records.rawValue)
    }
    
    func reloadRecords(records: [Records]) {
        self.clearRecords()
        UserDefaults.standard.set(encodable: records, forKey: Keys.records.rawValue)
    }
}
