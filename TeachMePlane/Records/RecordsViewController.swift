import UIKit

//MARK: - ENUMS

protocol RecordsViewControllerDelegate: AnyObject {
    func goToGameViewController()
}

class RecordsViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var nameVCLabel: UILabel?
    @IBOutlet weak var recordsTable: UITableView?
    @IBOutlet weak var recordsTableView: UIView?
    @IBOutlet weak var reloadButtonView: UIView?
    
    //MARK: - VARS
    weak var delegate: RecordsViewControllerDelegate?
    private var records: [Records]?
    var recordsAbsentAllertView = RecordsAbsentAlertView()
    
    //MARK: - LETS
    let highscoresNumber = 20
    
    //MARK: - Life functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadRecords()
        self.checkRecords()
    }
    
    //MARK: - IBActions
    @IBAction func goToMainMenuButtonPressed(_ sender: UIButton) {
        self.goToMainMenu()
    }
    
    @IBAction func clearRecordsButtonPressed(_ sender: UIButton) {
        self.clearRecords()
    }
    
    //MARK: - Flow functions
    private func goToMainMenu() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    private func loadRecords() {
        self.records = RecordsManager.shared.loadRecords()
    }
    
    private func checkRecords() {
        if self.records?.count ?? 0 > 0 {
            self.showRecordsView()
            self.sortRecords()
            self.limitHighscoresNumber()
        } else {
            self.showRecordsAbsentAllertView()
            self.addRecordsAbsentAllertView()
        }
    }
    
    private func showRecordsView() {
        self.recordsTableView?.isHidden = false
        self.reloadButtonView?.isHidden = false
    }
    
    private func showRecordsAbsentAllertView() {
        self.recordsTableView?.isHidden = true
        self.reloadButtonView?.isHidden = true
    }
    
    private func addRecordsAbsentAllertView() {
        self.recordsAbsentAllertView = RecordsAbsentAlertView.instanceFromNib()
        let recordsAbsentAllertViewWidth = self.view.frame.width * 3 / 4
        let recordsAbsentAllertViewHeight = self.view.frame.height * 1 / 4
        self.recordsAbsentAllertView.frame = CGRect(x: 0, y: 0, width: recordsAbsentAllertViewWidth, height: recordsAbsentAllertViewHeight)
        self.recordsAbsentAllertView.center = self.view.center
        self.recordsAbsentAllertView.delegate = self
        self.view.addSubview(self.recordsAbsentAllertView)
    }
    
    private func sortRecords() {
        guard var records = self.records else {
            return
        }
        records.sort(by: { $0.score ?? 0 > $1.score ?? 0})
        self.records = records
    }
    
    private func clearRecords() {
        self.records = []
        self.recordsTable?.reloadData()
        RecordsManager.shared.clearRecords()
        self.checkRecords()
    }
    
    private func limitHighscoresNumber() {
        if var records = self.records {
            if records.count > self.highscoresNumber {
                let firstIndex = self.highscoresNumber
                let lastIndex = records.count - 1
                for _ in firstIndex...lastIndex {
                    records.removeLast()
                }
                self.records = records
                RecordsManager.shared.reloadRecords(records: records)
                self.recordsTable?.reloadData()
            } else {
                return
            }
        }
    }
}

//MARK: - Extensions
extension RecordsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.records?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "RecordsTableViewCell", for: indexPath) as? RecordsTableViewCell,
              let records = self.records
        else {
            return UITableViewCell()
        }
        cell.numberLabel?.text = "\(indexPath.row + 1)"
        cell.configure(obgect: records[indexPath.row])
        return cell
    }
}

extension RecordsViewController: RecordsAbsentAlertViewDelegate {
    func playButtonPressed() {
        self.delegate?.goToGameViewController()
    }
    
    func goToMainMenuButtonPressed() {
        self.goToMainMenu()
    }
}
