import UIKit
import CoreMotion

//MARK: - ENUMS

class GameViewController: UIViewController {
    //MARK: - IBOutlets
    @IBOutlet weak var gameArea: UIView?
    @IBOutlet weak var planeArea: UIView?
    @IBOutlet weak var mainPlaneArea: UIView?
    @IBOutlet weak var grassArea: UIView?
    @IBOutlet weak var treeArea: UIView?
    @IBOutlet weak var cloudArea: UIView?
    @IBOutlet weak var scoreLabel: UILabel?
    @IBOutlet weak var lifesLabel: UILabel?
    @IBOutlet weak var statusBarArea: UIView?
    
    //MARK: - VARS
    private var mainPlane: UIImageView?
    private var movingManager = CMMotionManager()
    private var shakerManager = CMMotionManager()
    private var dangerousPlaneOnScreen: [UIImageView] = []
    private var scores = 0
    private var settings: Settings?
    private var nameMainPlane: String?
    private var playerName: String?
    private var difficulty: Difficulty?
    private var date: String?
    private var animateCloudInterval: Double = 4.5
    private var animateDangerousPlaneInterval: Double = 3.0
    private var groundSpeed:Double = 160
    private var addDangerousPlaneTimer = Timer()
    private var addTreeTimer = Timer()
    private var addCloudTimer = Timer()
    private var crashTimer = Timer()
    private var intersactionTimer = Timer()
    private var addGrassTimer = Timer()
    private var countScoreTimer = Timer()
    
    //MARK: - LETS
    private let trees = [UIImage(named: "Tree1"),
                         UIImage(named: "Tree2"),
                         UIImage(named: "Tree3"),
                         UIImage(named: "Tree4"),
                         UIImage(named: "Tree5"),
                         UIImage(named: "Tree6"),
                         UIImage(named: "Tree7"),
                         UIImage(named: "Tree8"),
                         UIImage(named: "Tree9"),
                         UIImage(named: "Tree10")
    ]
    
    private let clouds = [UIImage(named: "Cloud1"),
                          UIImage(named: "Cloud2"),
                          UIImage(named: "Cloud3"),
                          UIImage(named: "Cloud4"),
                          UIImage(named: "Cloud5")
    ]
    
    private let datePicker = UIDatePicker()
    private let treeNumberOnScreenAtTheSameTime: Double = 4
    private let cloudNumberOnScreenAtSameTime: Double = 2
    private let grassWidth: CGFloat = 85
    private let gameOverLabelInterval: Double = 3.0
    private let treeWidth: CGFloat = 85
    private let planeWidth: CGFloat = 80
    private let dangerousPlaneWidth: CGFloat = 80
    private let changeMainPlaneSizeDuration: Double = 1
    private let invulnerabilityMainPlaneInterval: Double = 2
    
    //MARK: - Life functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadSettings()
        self.addStartGrassBackground()
        self.addTreeBackground()
        self.addCloudBackground()
        self.addDangerousPlaneBackground()
        self.checkIntersectsMainAndDangerousPlanes()
        self.countScores()
        self.addGrassBackground()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.addMainPlane()
    }
    
    //MARK: - IBActions
    @IBAction func goToMainMenu(_ sender: UIButton) {
        self.safeRecords()
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func handlePan(_ recognizer: UIPanGestureRecognizer) {
        guard let mainPlane = self.mainPlane else {
            print("ERROR: There is no Main Plane")
            return
        }
        let translation = recognizer.translation(in: mainPlane)
        mainPlane.center = CGPoint (x: mainPlane.center.x + translation.x,y: mainPlane.center.y)
        recognizer.setTranslation(CGPoint.zero, in: mainPlane)
    }
    
    //MARK: - Flow functions
    private func addGestureRecognizerForMainPlane() {
        guard let mainPlane = self.mainPlane else {
            print("ERROR: There is no Main Plane")
            return
        }
        let recognizer = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        mainPlane.isUserInteractionEnabled = true
        mainPlane.addGestureRecognizer(recognizer)
    }
    
    private func moveMainPlane() {
        if self.movingManager.isAccelerometerAvailable {
            self.movingManager.accelerometerUpdateInterval = 0.01
            self.movingManager.startAccelerometerUpdates(to: .main) { [weak self] (data: CMAccelerometerData?, error: Error?) in
                if let acceleration = data?.acceleration {
                    switch acceleration.x {
                    case 0.2...0.4 :
                        self?.mainPlane?.frame.origin.x += 0.5
                    case 0.4...0.6 :
                        self?.mainPlane?.frame.origin.x += 1.25
                    case let x where x > 0.6 :
                        self?.mainPlane?.frame.origin.x += 2
                    case -0.4 ... -0.2 :
                        self?.mainPlane?.frame.origin.x -= 0.5
                    case -0.6 ... -0.4 :
                        self?.mainPlane?.frame.origin.x -= 1.25
                    case let x where x < -0.6 :
                        self?.mainPlane?.frame.origin.x -= 2
                    default:
                        return
                    }
                    self?.checkMainPlaneBehindScreen()
                }
            }
        }
    }
    
    private func checkMainPlaneBehindScreen() {
        guard let mainPlane = self.mainPlane else {
            print("ERROR: There is No Main Plane!")
            return
        }
        switch mainPlane.frame.origin.x {
        case ...0 :
            mainPlane.frame.origin.x = 0
        case (self.view.frame.width - mainPlane.frame.width)... :
            mainPlane.frame.origin.x = (self.view.frame.width - mainPlane.frame.width)
        default:
            return
        }
    }
    
    private func shakeMainPlane() {
        if self.shakerManager.isDeviceMotionAvailable {
            self.shakerManager.gyroUpdateInterval = 0.1
            self.shakerManager.startGyroUpdates(to: .main) { (data, error) in
                if let rate = data?.rotationRate {
                    if rate.x > 5 || rate.y > 5 || rate.z > 5 {
                        self.invulnerabilityMainPlane()
                    }
                }
            }
        }
    }
    
    private func invulnerabilityMainPlane() {
        self.intersactionTimer.invalidate()
        self.shakerManager.stopGyroUpdates()
        self.changeMainPlaneSize(coefficient: 1.2)
        let invulnerabilityTimer = Timer.scheduledTimer(withTimeInterval: self.invulnerabilityMainPlaneInterval, repeats: false) { (internalTimer) in
            self.checkIntersectsMainAndDangerousPlanes()
            self.shakeMainPlane()
            self.changeMainPlaneSize()
        }
    }
    
    private func changeMainPlaneSize(coefficient: CGFloat = 1) {
        guard let mainPlane = self.mainPlane else {
            print("ERROR: There is No Main Plane!")
            return
        }
        let newPlaneWidth = self.planeWidth * coefficient
        let newPlaneHeight = self.planeWidth * coefficient
        let biasX = (newPlaneWidth - mainPlane.frame.width) / 2
        let biasY = newPlaneHeight - mainPlane.frame.height
        let origin = mainPlane.frame.origin
        let newOriginX = origin.x - biasX
        let newOriginY = origin.y - biasY
        UIView.animate(withDuration: self.changeMainPlaneSizeDuration) {
            mainPlane.frame = CGRect(x: newOriginX, y: newOriginY, width: newPlaneWidth, height: newPlaneHeight)
        }
    }
    
    private func addMainPlane() {
        guard let mainPlaneArea = self.mainPlaneArea else {
            print("ERROR: There is no Main Plane Area!")
            return
        }
        let planeHight = self.planeWidth
        let originX = mainPlaneArea.frame.size.width / 2 - planeWidth / 2
        let originY = mainPlaneArea.frame.size.height - planeHight
        self.mainPlane = UIImageView()
        self.mainPlane?.frame = CGRect(x: originX, y: originY, width: planeWidth, height: planeHight)
        self.mainPlane?.backgroundColor = .clear
        self.mainPlane?.image = UIImage(named: self.nameMainPlane ?? SettingsManager.shared.defaultMainPlane)
        self.mainPlane?.contentMode = .scaleToFill
        
        guard let mainPlane = self.mainPlane else {
            print("ERROR: There is no Main Plane")
            return
        }
        mainPlaneArea.addSubview(mainPlane)
        self.chooseControlType()
    }
    
    private func chooseControlType() {
        switch self.settings?.controlType {
        case ControlTypes.accelerometer.rawValue:
            self.moveMainPlane()
            self.shakeMainPlane()
        case ControlTypes.handle.rawValue:
            self.addGestureRecognizerForMainPlane()
        default:
            print("ERROR with choosing Control Type")
        }
    }
    
    private func addDangerousPlane() {
        guard let planeArea = self.planeArea else {
            print("ERROR: There is no Plane Area!")
            return
        }
        let planeWidth = Double(self.dangerousPlaneWidth)
        let planeHight = planeWidth
        let originX: Double = .random(in: 0...Double(planeArea.frame.width) - planeWidth)
        let originY: Double = -planeHight
        let dangerousPlane = UIImageView()
        dangerousPlane.frame = CGRect(x: originX, y: originY, width: planeWidth, height: planeHight)
        dangerousPlane.backgroundColor = .clear
        dangerousPlane.image = UIImage(named: "DangerousPlane")
        dangerousPlane.contentMode = .scaleToFill
        self.animateDangerousPlane(dangerousPlane: dangerousPlane)
        self.dangerousPlaneOnScreen.append(dangerousPlane)
        planeArea.addSubview(dangerousPlane)
    }
    
    private func addDangerousPlaneBackground() {
        let dangerousPlaneNumberOnScreenAtSameTime = self.settings?.difficulty?.dangerousPlaneNumber ?? 1
        let addDangerousPlaneInterval = self.animateDangerousPlaneInterval / dangerousPlaneNumberOnScreenAtSameTime
        self.addDangerousPlaneTimer = Timer.scheduledTimer(withTimeInterval: addDangerousPlaneInterval, repeats: true) { (internalTimer) in
            self.addDangerousPlane()
        }
    }
    
    private func animateDangerousPlane(dangerousPlane: UIImageView) {
        guard let planeArea = self.planeArea else {
            print("ERROR: There is no Plane Area!")
            return
        }
        UIView.animate(withDuration: self.animateDangerousPlaneInterval, delay: 0, options: .curveLinear) {
            dangerousPlane.frame.origin.y = planeArea.frame.height
        } completion: { (true) in
            self.removeDangerousPlaneIfUnderTheScreen()
        }
    }
    
    private func removeDangerousPlaneIfUnderTheScreen() {
        var index = self.dangerousPlaneOnScreen.startIndex
        for dangerousPlane in self.dangerousPlaneOnScreen {
            if dangerousPlane.layer.presentation()?.frame.origin.y == self.planeArea?.frame.height {
                dangerousPlane.removeFromSuperview()
                self.dangerousPlaneOnScreen.remove(at: index)
            } else {
                index += 1
            }
        }
    }
    
    private func addTree(treeImageNumber: Int) {
        guard let treeArea = self.treeArea else {
            print("ERROR: There is no Tree Area!")
            return
        }
        let treeHeight = self.treeWidth
        let originY = -treeHeight
        let originX: CGFloat = .random(in: 0...treeArea.frame.size.width - treeWidth)
        let tree = UIImageView()
        tree.frame = CGRect(x: originX, y: originY, width: self.treeWidth, height: treeHeight)
        tree.backgroundColor = .clear
        tree.image = self.trees[treeImageNumber]
        self.animateTreesBackground(tree: tree)
        treeArea.addSubview(tree)
    }
    
    private func addCloud(cloudImageNumber: Int) {
        guard let cloudArea = self.cloudArea else {
            print("ERROR: There is no Cloud Area!")
            return
        }
        let cloudWidth:Double = 250
        let cloudHeight = cloudWidth
        let originY = -cloudHeight
        let originX: Double = .random(in: 0...Double(cloudArea.frame.size.width) - cloudWidth)
        let cloud = UIImageView()
        cloud.frame = CGRect(x: originX, y: originY, width: cloudWidth, height: cloudHeight)
        cloud.backgroundColor = .clear
        cloud.image = self.clouds[cloudImageNumber]
        self.animateCloudsBackground(cloud: cloud)
        cloudArea.addSubview(cloud)
    }
    
    private func addTreeBackground() {
        guard let treeArea = self.treeArea else {
            print("ERROR: There is no Tree Area!")
            return
        }
        var treeImageNumberForFunc = self.trees.startIndex
        let treeHeight = self.treeWidth
        let animateTreeInterval = Double(treeArea.frame.height) / self.groundSpeed
        let addTreeInterval = animateTreeInterval / self.treeNumberOnScreenAtTheSameTime
        self.addTreeTimer = Timer.scheduledTimer(withTimeInterval: addTreeInterval, repeats: true) { (internalTimer) in
            self.addTree(treeImageNumber: treeImageNumberForFunc)
            if treeImageNumberForFunc < self.trees.count - 1 { treeImageNumberForFunc += 1
            } else {
                treeImageNumberForFunc = self.trees.startIndex
            }
        }
    }
    
    private func addCloudBackground() {
        var cloudImageNumberForFunc = self.clouds.startIndex
        let addCloudInterval = self.animateCloudInterval / self.cloudNumberOnScreenAtSameTime
        self.addCloudTimer = Timer.scheduledTimer(withTimeInterval: addCloudInterval, repeats: true) { (internalTimer) in
            self.addCloud(cloudImageNumber: cloudImageNumberForFunc)
            if cloudImageNumberForFunc < self.clouds.count - 1 { cloudImageNumberForFunc += 1
            } else {
                cloudImageNumberForFunc = self.clouds.startIndex
            }
        }
    }
    
    private func animateTreesBackground(tree: UIImageView) {
        guard let treeArea = self.treeArea else {
            print("ERROR: There is no Game Area!")
            return
        }
        let treeHeight = self.treeWidth
        let animateTreeInterval = Double(treeArea.frame.height + treeHeight) / self.groundSpeed
        UIView.animate(withDuration: animateTreeInterval, delay: 0, options: .curveLinear) {
            tree.frame.origin.y = treeArea.frame.height
        } completion: { (true) in
            tree.removeFromSuperview()
        }
    }
    
    private func animateCloudsBackground(cloud: UIImageView) {
        guard let cloudArea = self.cloudArea else {
            print("ERROR: There is no Cloud Area!")
            return
        }
        let addCloudInterval = self.animateCloudInterval / self.cloudNumberOnScreenAtSameTime
        UIView.animate(withDuration: self.animateCloudInterval, delay: 0, options: .curveLinear) {
            cloud.frame.origin.y = cloudArea.frame.height
        } completion: { (true) in
            cloud.removeFromSuperview()
        }
    }
    
    private func crash() {
        self.stopTimers()
        self.stopAllAnimations()
        self.movingManager.stopAccelerometerUpdates()
        self.shakerManager.stopGyroUpdates()
        self.mainPlane?.isUserInteractionEnabled = false
        self.createGameOverLabel()
        self.safeRecords()
        self.crashTimer = Timer.scheduledTimer(withTimeInterval: self.gameOverLabelInterval, repeats: false) { (internalTimer) in
            self.navigationController?.popToRootViewController(animated: true)
            internalTimer.invalidate()
        }
    }
    
    private func createGameOverLabel() {
        guard let gameArea = self.gameArea else {
            print("ERROR: There is no Game Area!")
            return
        }
        let gameOverLabelWidth = Double(gameArea.frame.width)
        let gameOverLabelHeight:Double = 100
        let gameOverLabelOriginY = Double(self.view.frame.height / 2) - gameOverLabelHeight / 2
        let gameOverLabel = UILabel()
        gameOverLabel.frame = CGRect(x: 0, y: gameOverLabelOriginY, width: gameOverLabelWidth, height: gameOverLabelHeight)
        gameOverLabel.backgroundColor = .red
        let localizedString = "GAME OVER".localized
        gameOverLabel.text = localizedString
        gameOverLabel.font = gameOverLabel.font.withSize(40)
        gameOverLabel.textAlignment = .center
        self.view.addSubview(gameOverLabel)
    }
    
    private func checkIntersectsMainAndDangerousPlanes() {
        self.intersactionTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { (internalTimer) in
            if let mainPlane = self.mainPlane {
                for plane in self.dangerousPlaneOnScreen {
                    if let mainPlanePresentation = mainPlane.layer.presentation(),
                       let planePresentation = plane.layer.presentation(),
                       mainPlanePresentation.frame.intersects(planePresentation.frame) {
                        self.crash()
                    }
                }
            }
        }
    }
    
    private func createGrass(originX: CGFloat, originY: CGFloat) {
        guard let grassArea = self.grassArea else {
            print("ERROR: There is no Grass Area!")
            return
        }
        let grassHeight = self.grassWidth
        let grass = UIImageView()
        grass.frame = CGRect(x: originX, y: originY, width: self.grassWidth, height: grassHeight)
        grass.backgroundColor = .clear
        grass.image = UIImage(named: "Grass")
        grass.contentMode = .scaleToFill
        grassArea.addSubview(grass)
        self.animateGrassBackground(grass: grass)
    }
    
    private func addGrassBackground() {
        guard let grassArea = self.grassArea else {
            print("ERROR: There is no Grass Area!")
            return
        }
        let grassHeight = self.grassWidth
        let grassInLine = Int(grassArea.frame.width / self.grassWidth)
        let grassInColumn = Int(grassArea.frame.height / grassHeight) + 1
        let animateGrassInterval = (Double(grassArea.frame.height)) / self.groundSpeed
        let originY = -grassHeight
        let addGrassInterval = animateGrassInterval / Double(grassInColumn)
        self.addGrassTimer = Timer.scheduledTimer(withTimeInterval: addGrassInterval, repeats: true) { (internalTimer) in
            var originX: CGFloat = 0
            for _ in 0...grassInLine {
                self.createGrass(originX: originX, originY: originY)
                originX += self.grassWidth
            }
        }
    }
    
    private func addStartGrassBackground() {
        guard let grassArea = self.grassArea else {
            print("ERROR: There is no Grass Area!")
            return
        }
        let grassHeight = self.grassWidth
        let grassInLine = Int(grassArea.frame.width / self.grassWidth)
        let grassInColumn = Int(grassArea.frame.height / grassHeight)
        var originX: CGFloat = 0
        var originY = -grassHeight
        for _ in 0...grassInColumn {
            for _ in 0...grassInLine {
                self.createGrass(originX: originX, originY: originY)
                originX += self.grassWidth
            }
            originY += grassHeight
            originX = 0
        }
    }
    
    private func animateGrassBackground(grass: UIImageView) {
        guard let grassArea = self.grassArea else {
            print("ERROR: There is no Grass Area!")
            return
        }
        let grassHeight = CGFloat(self.grassWidth)
        let grassAreaHeight = grassArea.frame.height
        let distance = grassHeight + grassAreaHeight
        let animateGrassInterval = (grassArea.frame.height + grassHeight) / CGFloat(self.groundSpeed)
        UIView.animate(withDuration: TimeInterval(animateGrassInterval), delay: 0, options: .curveLinear) {
            grass.frame.origin.y += distance
        } completion: { (true) in
            grass.removeFromSuperview()
        }
    }
    
    private func loadSettings() {
        self.settings = SettingsManager.shared.loadSettings()
        self.reloadGameSpeed()
        self.writeNameMainPlane()
        self.writePlayerName()
    }
    
    private func writeNameMainPlane() {
        self.nameMainPlane = self.settings?.mainPlane
    }
    
    private func writePlayerName() {
        self.playerName = self.settings?.playerName
    }
    
    private func reloadGameSpeed() {
        let gameSpeedCoefficient = self.settings?.difficulty?.gameSpeedCoefficient ?? 1
        self.animateCloudInterval = self.animateCloudInterval / gameSpeedCoefficient
        self.animateDangerousPlaneInterval = self.animateDangerousPlaneInterval / gameSpeedCoefficient
        self.groundSpeed = self.groundSpeed * gameSpeedCoefficient
    }
    
    private func countScores() {
        let scoresTimeInterval = self.settings?.difficulty?.scoresTimeInterval ?? 1.5
        self.countScoreTimer = Timer.scheduledTimer(withTimeInterval: scoresTimeInterval, repeats: true) { (internalTimer) in
            self.scores += 1
            let localizedString = "Score: ".localized
            self.scoreLabel?.text = localizedString + "\(self.scores)"
        }
    }
    
    private func safeRecords() {
        self.writeDownDate()
        let currentRecord = Records(playerName: self.playerName, score: self.scores, date: self.date)
        RecordsManager.shared.saveRecords(currentRecord)
    }
    
    private func stopTimers() {
        self.addDangerousPlaneTimer.invalidate()
        self.addTreeTimer.invalidate()
        self.addCloudTimer.invalidate()
        self.intersactionTimer.invalidate()
        self.addGrassTimer.invalidate()
        self.countScoreTimer.invalidate()
    }
    
    private func stopAnimations(superview: UIView?) {
        guard let superview = superview else {return}
        superview.subviews.forEach({$0.layer.removeAllAnimations()})
    }
    
    private func stopAllAnimations() {
        self.view.subviews.map({ pauseLayer(layer: $0.layer) })
    }
    
    private func pauseLayer(layer: CALayer) {
        let pausedTime: CFTimeInterval = layer.convertTime(CACurrentMediaTime(), from: nil)
        layer.speed = 0.0
        layer.timeOffset = pausedTime
    }
    
    private func writeDownDate() {
        let date = self.datePicker.date
        let formatter = DateFormatter()
        formatter.dateFormat = "dd-MM-yyyy HH:mm"
        self.date = formatter.string(from: date)
    }
    
}
